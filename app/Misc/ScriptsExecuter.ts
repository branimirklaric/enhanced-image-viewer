﻿namespace Misc {
    /**
     * Injects script into the specified tab and calls the callback.
     */
    export type InjectScript =
        (tabId: number, script: chrome.tabs.InjectDetails, callback: () => void) => void;

    export class ScriptInjector {
        /**
         * Used for injecting a list of scripts into a tab.
         * @param injectScript Injects script into the specified tab and calls the callback.
         * @param scriptsToInject Files to inject into the tab.
         */
        constructor(
            private injectScript: InjectScript,
            private scriptsToInject: chrome.tabs.InjectDetails[]) {
        }

        /**
         * Injects files into the tab specified by tab ID and executes the callback after all files have been injected. The files are injected from first to last.
         * @param tabId Tab ID of the tab in which to inject the files.
         * @param callback Function to call after all the files were injected.
         */
        injectScripts(
            tabId: number,
            callback: () => void) {
            //I have to slice it first to copy it in order not to reverse
            //the source array, because reverse mutates the array in place.
            let reversedScripts = this.scriptsToInject.slice().reverse();
            let chainedCallbacks = _.reduce(reversedScripts, (callback, injectDetails) => {
                return () => this.injectScript(tabId, injectDetails, callback);
            }, callback);
            chainedCallbacks();
        }
    }
}
