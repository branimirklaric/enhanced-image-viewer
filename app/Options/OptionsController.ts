﻿namespace Options {
    export class OptionsController {
        constructor(private optionsRepository: OptionsRepository) {
        }

        findAllOptions(callback: (options: Options) => void) {
            this.optionsRepository.findAllOptions(callback);
        }

        updateBackgroundColor(backgroundColor: string) {
            this.optionsRepository.updateBackgroundColor(backgroundColor);
        }

        updateStartViewingMode(startViewingMode: string) {
            this.optionsRepository.updateStartViewingMode(parseInt(startViewingMode));
        }

        updateImagePosition(imagePosition: Core.Enums.ImagePosition) {
            this.optionsRepository.updateImagePosition(imagePosition);
        }

        updateHideScrollbars(hideScrollbars: boolean) {
            this.optionsRepository.updateHideScrollbars(hideScrollbars);
        }

        updateVerticallyKey(oldKeys: Core.MousewheelKeys, newKey: number) {
            oldKeys.scrollVerticallyKey = newKey;
            this.optionsRepository.updateMousewheelKeys(oldKeys);
        }

        updateHorizontallyKey(oldKeys: Core.MousewheelKeys, newKey: number) {
            oldKeys.scrollHorizontallyKey = newKey;
            this.optionsRepository.updateMousewheelKeys(oldKeys);
        }

        updateRotateKey(oldKeys: Core.MousewheelKeys, newKey: number) {
            oldKeys.rotateImageKey = newKey;
            this.optionsRepository.updateMousewheelKeys(oldKeys);
        }

        updateZoomMode(zoomMode: Core.Enums.ZoomMode) {
            this.optionsRepository.updateZoomMode(zoomMode);
        }

        updateShowContextMenu(showContextMenu: boolean) {
            this.optionsRepository.updateShowContextMenu(showContextMenu);
        }
    }
}
