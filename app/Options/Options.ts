﻿namespace Options {
    export interface Options {
        backgroundColor: string;
        startViewingMode: Core.Enums.StartViewingMode;
        imagePosition: Core.Enums.ImagePosition;
        hideScrollbars: boolean;
        mousewheelKeys: Core.MousewheelKeys;
        zoomMode: Core.Enums.ZoomMode;
        showContextMenu: boolean;
    }
}
