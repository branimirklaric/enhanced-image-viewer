﻿namespace Options {
    export class OptionsRepository {
        /**Option keys with default values if the key is missing from the storage.*/
        public static readonly defaultOptions: Options = {
            backgroundColor: "FFFFFF",
            startViewingMode: Core.Enums.StartViewingMode.FillWindowIfLarger,
            imagePosition: Core.Enums.ImagePosition.Center,
            hideScrollbars: false,
            mousewheelKeys: {
                scrollVerticallyKey: 17, //ctrl
                scrollHorizontallyKey: 16, //shift
                rotateImageKey: 18 //alt
            },
            zoomMode: Core.Enums.ZoomMode.Enhanced,
            showContextMenu: true
        };

        constructor(private syncStorage: chrome.storage.SyncStorageArea) {
        }

        findAllOptions(callback: (options: Options) => void) {
            this.syncStorage.get(OptionsRepository.defaultOptions, callback);
        }

        updateBackgroundColor(backgroundColor: string) {
            this.syncStorage.set({ backgroundColor: backgroundColor });
        }

        updateStartViewingMode(startViewingMode: Core.Enums.StartViewingMode) {
            this.syncStorage.set({ startViewingMode: startViewingMode });
        }

        updateImagePosition(imagePosition: Core.Enums.ImagePosition) {
            this.syncStorage.set({ imagePosition: imagePosition });
        }

        updateHideScrollbars(hideScrollbars: boolean) {
            this.syncStorage.set({ hideScrollbars: hideScrollbars });
        }

        updateMousewheelKeys(mousewheelKeys: Core.MousewheelKeys) {
            this.syncStorage.set({ mousewheelKeys: mousewheelKeys })
        }

        updateZoomMode(zoomMode: Core.Enums.ZoomMode) {
            this.syncStorage.set({ zoomMode: zoomMode });
        }

        updateShowContextMenu(showContextMenu: boolean) {
            this.syncStorage.set({ showContextMenu: showContextMenu });
        }
    }
}
