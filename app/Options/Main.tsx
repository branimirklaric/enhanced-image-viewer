﻿namespace Options {
    export class Main {
        static main() {
            let optionsController =
                new Options.OptionsController(
                    new Options.OptionsRepository(chrome.storage.sync)
                );
            optionsController.findAllOptions(options => {
                ReactDOM.render(
                    <OptionsView
                        optionsController={optionsController}
                        options={options}
                    />,
                    document.getElementById("container")
                );
            });
        }
    }
}

$(document).ready(Options.Main.main);
