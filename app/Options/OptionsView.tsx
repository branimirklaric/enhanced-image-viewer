namespace Options {
    export interface OptionsViewProps {
        optionsController: OptionsController,
        options: Options
    }

    export class OptionsView extends React.Component<OptionsViewProps, {}> {
        public backgroundColor: View.BackgroundColorComponent;
        public startViewingMode: View.StartViewingModeComponent;
        public imagePosition: View.ImagePositionComponent;
        public hideScrollbars: View.HideScrollbarsComponent;
        public mousewheelKeys: View.MousewheelKeysComponent;
        public zoomMode: View.ZoomModeComponent;
        public showContextMenu: View.ShowContextMenuComponent;

        private controller: OptionsController;

        constructor(props: OptionsViewProps) {
            super(props);
            this.controller = props.optionsController;
        }

        render() {
            const textIndent = "1.5em";
            return (
                <div>
                    <View.BackgroundColorComponent
                        backgroundColor={this.props.options.backgroundColor}
                        updateBackgroundColor={(color) =>
                            this.controller.updateBackgroundColor(color)
                        }
                        ref={e => this.backgroundColor = e}
                    />
                    <View.StartViewingModeComponent
                        startViewingMode={this.props.options.startViewingMode}
                        updateStartViewingMode={(mode) =>
                            this.controller.updateStartViewingMode(mode)
                        }
                        ref={e => this.startViewingMode = e}
                    />
                    <View.ImagePositionComponent
                        checkedPosition={this.props.options.imagePosition}
                        updateImagePosition={(position) =>
                            this.controller.updateImagePosition(position)
                        }
                        ref={e => this.imagePosition = e}
                    />
                    <View.HideScrollbarsComponent
                        scrollbarsHidden={this.props.options.hideScrollbars}
                        updateHideScrollbars={(hidden) =>
                            this.controller.updateHideScrollbars(hidden)
                        }
                        ref={e => this.hideScrollbars = e}
                    />
                    <View.MousewheelKeysComponent
                        mousewheelKeys={this.props.options.mousewheelKeys}
                        updateVerticallyKey={(old, neww) =>
                            this.controller.updateVerticallyKey(old, neww)
                        }
                        updateHorizontallyKey={(old, neww) =>
                            this.controller.updateHorizontallyKey(old, neww)
                        }
                        updateRotateKey={(old, neww) =>
                            this.controller.updateRotateKey(old, neww)
                        }
                        textIndent={textIndent}
                        ref={e => this.mousewheelKeys = e}
                    />
                    <View.ZoomModeComponent
                        zoomMode={this.props.options.zoomMode}
                        updateZoomMode={(zoomMode) =>
                            this.controller.updateZoomMode(zoomMode)
                        }
                        textIndent={textIndent}
                        ref={e => this.zoomMode = e}
                    />
                    <View.ShowContextMenuComponent
                        showContextMenu={this.props.options.showContextMenu}
                        updateShowContextMenu={(showMenu) =>
                            this.controller.updateShowContextMenu(showMenu)
                        }
                        ref={e => this.showContextMenu = e}
                    />
                </div>
            );
        }
    }
}
