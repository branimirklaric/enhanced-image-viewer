namespace Options.View {
    export interface ImagePositionProps {
        checkedPosition: Core.Enums.ImagePosition,
        updateImagePosition: (imagePosition: Core.Enums.ImagePosition) => void
    }

    interface ImagePositionState {
        checkedPosition: Core.Enums.ImagePosition,
    }

    let CEI = Core.Enums.ImagePosition;

    export class ImagePositionComponent
        extends React.Component<ImagePositionProps, ImagePositionState> {
        constructor(props: ImagePositionProps) {
            super(props);
            this.state = { checkedPosition: props.checkedPosition };
        }

        onChange(event: React.FormEvent<HTMLFormElement>) {
            let newCheckedPosition = Math.abs(this.state.checkedPosition - 1);
            this.props.updateImagePosition(newCheckedPosition);
            super.setState({ checkedPosition: newCheckedPosition });
        }

        render() {
            return (
                <Wrapper>
                    <form onChange={e => this.onChange(e)}>
                        <Label>Image position</Label>
                        <input
                            type="radio" name="ImagePosition" value="0"
                            checked={this.state.checkedPosition === CEI.TopLeftCorner}
                            style={{ marginRight: 5 }}
                        />
                        Top left corner &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input
                            type="radio" name="ImagePosition" value="1"
                            checked={this.state.checkedPosition === CEI.Center}
                            style={{ marginRight: 5 }}
                        />
                        Center
                    </form>
                </Wrapper>
            );
        }
    }
}
