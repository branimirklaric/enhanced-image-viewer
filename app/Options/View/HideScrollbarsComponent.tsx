namespace Options.View {
    export interface HideScrollbarsProps {
        scrollbarsHidden: boolean,
        updateHideScrollbars: (hiddenScrollbars: boolean) => void
    }

    interface HideScrollbarsState {
        scrollbarsHidden: boolean
    }

    export class HideScrollbarsComponent
        extends React.Component<HideScrollbarsProps, HideScrollbarsState> {
        constructor(props: HideScrollbarsProps) {
            super(props);
            this.state = { scrollbarsHidden: props.scrollbarsHidden };
        }

        onChange(event: React.FormEvent<HTMLFormElement>) {
            this.props.updateHideScrollbars(!this.state.scrollbarsHidden);
            super.setState({ scrollbarsHidden: !this.state.scrollbarsHidden });
        }

        render() {
            return (
                <Wrapper>
                    <form onChange={e => this.onChange(e)}>
                        <Label>Hide scrollbars</Label>
                        <input type="checkbox" checked={this.state.scrollbarsHidden} />
                    </form>
                </Wrapper>
            );
        }
    }
}
