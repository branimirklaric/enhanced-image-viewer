namespace Options.View {
    export interface StartViewingModeProps {
        startViewingMode: Core.Enums.StartViewingMode
        updateStartViewingMode: ((startViewingMode: string) => void)
    }

    export class StartViewingModeComponent
        extends React.Component<StartViewingModeProps, { mode: string }> {
        constructor(props: StartViewingModeProps) {
            super(props);
            this.state = { mode: props.startViewingMode.toString() };
        }

        updateStartViewingMode(event: React.ChangeEvent<HTMLSelectElement>) {
            this.props.updateStartViewingMode(event.target.value);
            super.setState({ mode: event.target.value });
        }

        render() {
            return (
                <Wrapper>
                    <Label>Default size</Label>
                    <select
                        onChange={this.updateStartViewingMode.bind(this)}
                        value={this.state.mode}>
                        <option value="0">Actual size</option>
                        <option value="1">Fill window</option>
                        <option value="2">Fill window if larger</option>
                    </select>
                </Wrapper>
            );
        }
    }
}
