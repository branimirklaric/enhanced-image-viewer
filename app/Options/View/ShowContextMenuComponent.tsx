namespace Options.View {
    export interface ShowContextMenuProps {
        showContextMenu: boolean,
        updateShowContextMenu: (showContextMenu: boolean) => void
    }

    interface ShowContextMenuState {
        showContextMenu: boolean
    }

    export class ShowContextMenuComponent
        extends React.Component<ShowContextMenuProps, ShowContextMenuState> {
        constructor(props: ShowContextMenuProps) {
            super(props);
            this.state = { showContextMenu: props.showContextMenu };
        }

        onChange(event: React.FormEvent<HTMLFormElement>) {
            this.props.updateShowContextMenu(!this.state.showContextMenu);
            super.setState({ showContextMenu: !this.state.showContextMenu });
            chrome.runtime.sendMessage({ showContextMenu: !this.state.showContextMenu });
        }

        render() {
            return (
                <Wrapper>
                    <div style={{ background: "rgba(120, 255, 120, 0.81)" }}>
                        *NEW*
                        <form onChange={e => this.onChange(e)}>
                            <Label>Show "View image in same tab" context menu item</Label>
                            <input type="checkbox" checked={this.state.showContextMenu} />
                        </form>
                        <br />
                        Scroll down for more :)
                    </div>
                </Wrapper>
            );
        }
    }
}
