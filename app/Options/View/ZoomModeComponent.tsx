namespace Options.View {
    export interface ZoomModeProps extends TextIndent {
        zoomMode: Core.Enums.ZoomMode,
        updateZoomMode: (zoomMode: Core.Enums.ZoomMode) => void
    }

    interface ZoomModeState {
        zoomMode: Core.Enums.ZoomMode
    }

    let CEZ = Core.Enums.ZoomMode;

    export class ZoomModeComponent
        extends React.Component<ZoomModeProps, ZoomModeState> {
        constructor(props: ZoomModeProps) {
            super(props);
            this.state = { zoomMode: props.zoomMode };
        }

        onChange(event: React.FormEvent<HTMLFormElement>) {
            let newZoomMode = Math.abs(this.state.zoomMode - 1);
            this.props.updateZoomMode(newZoomMode);
            super.setState({ zoomMode: newZoomMode });
        }

        render() {
            return (
                <Wrapper>
                    <div style={{ background: "rgba(120, 255, 120, 0.81)" }}>
                        *NEW*
                        <form onChange={event => this.onChange(event)}>
                            <Label>Zoom mode</Label>
                            <input
                                type="radio" name="ZoomMode" value="0"
                                checked={this.state.zoomMode === CEZ.Default}
                                style={{ marginRight: 5 }}
                            />
                            Chrome default &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <input
                                type="radio" name="ZoomMode" value="1"
                                checked={this.state.zoomMode === CEZ.Enhanced}
                                style={{ marginRight: 5 }}
                            />
                            Enhanced
                        </form>
                        <div style={{ marginLeft: this.props.textIndent }}>
                            Chrome default zoom mode uses:
                            <ul style={{ marginTop: "0px" }}>
                                <li>Mousewheel for vertical scrolling.</li>
                                <li>"Scroll vertically" key + mousewheel for zooming.</li>
                            </ul>
                            Enhanced zoom mode uses:
                            <ul style={{ marginTop: "0px" }}>
                                <li>Mousewheel for zooming.</li>
                                <li>"Scroll vertically" key + mousewheel for vertical scrolling.</li>
                            </ul>
                        </div>
                    </div>
                </Wrapper>
            );
        }
    }
}
