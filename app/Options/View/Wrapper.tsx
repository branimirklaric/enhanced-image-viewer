namespace Options.View {
    export class Wrapper extends React.Component<{}, {}> {
        render() {
            return (<div style={{ marginBottom: 10 }}>{this.props.children}</div>);
        }
    }
}
