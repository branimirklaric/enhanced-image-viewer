namespace Options.View {
    export class Label extends React.Component<{}, {}> {
        render() {
            return (<span style={{ marginRight: 10 }}>{this.props.children}:</span>);
        }
    }
}
