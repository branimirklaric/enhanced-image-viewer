declare var jscolor: any;

namespace Options.View {
    export interface BackgroundColorProps {
        backgroundColor: string
        updateBackgroundColor: ((backgroundColor: string) => void),
    }

    export class BackgroundColorComponent extends React.Component<BackgroundColorProps, {}> {
        onChange(event: Event) {
            this.props.updateBackgroundColor(
                (event.target as HTMLInputElement).value
            );
        }

        inputRef(input: HTMLInputElement) {
            new jscolor(input);
            //I have to register it manually because React won't detect otherwise
            //because the events are sent from a jquery addon
            //or something like that.
            input.addEventListener("change", (event) => this.onChange(event));
        }

        render() {
            return (
                <Wrapper>
                    <Label>
                        Background color
                </Label>
                    <input
                        value={this.props.backgroundColor}
                        ref={input => this.inputRef(input)}
                    />
                    <br />
                </Wrapper>
            );
        }
    }

    /*export function BackgroundColorComponent(props: BackgroundColorProps) {
        function onChange(event: Event) {
            props.updateBackgroundColor(
                (event.target as HTMLInputElement).value
            );
        }

        function inputRef(input: HTMLInputElement) {
            new jscolor(input);
            //I have to register it manually because React won't detect otherwise
            //because the events are sent from a jquery addon
            //or something like that
            input.addEventListener("change", onChange);
        }

        return (
            <Wrapper>
                <Label>
                    Background color
                </Label>
                <input
                    value={props.backgroundColor}
                    ref={inputRef}
                />
                <br />
            </Wrapper>
        );
    }*/
}
