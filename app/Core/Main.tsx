﻿namespace Core {
    export class Main {
        private findExistingImage(imageSrc: string) {
            //Cannot specify src in the selector,
            //because it can have its protocol or root url missing.
            //element.src contains the full URL.
            return $("img").filter((_, element: HTMLImageElement) => {
                return element.src === imageSrc;
            })[0] as HTMLImageElement;
        }

        private replaceDocument(
            image: HTMLImageElement, backgroundColor: string) {
            let replacementHead = `<head><title>${image.src}</title></head>`;
            let replacementBody =
                `<body style='background: ${"#" + backgroundColor}; margin: 0'>`
                + "<div id='container'></div>"
                + "</body>";
            $("html").html(replacementHead + replacementBody);
        }

        private startEnhancementReact(image: HTMLImageElement) {
            let optionsRepository = new Options.OptionsRepository(chrome.storage.sync);
            optionsRepository.findAllOptions((options) => {
                this.replaceDocument(image, options.backgroundColor);
                ReactDOM.render(
                    <ImageView image={image} options={options} />,
                    document.getElementById("container")
                );
            });
        }

        onMessageListener(message: Message) {
            if (message.startEnhancement !== true) {
                return;
            }
            if ($("img").length === 0) {
                return;
            }
            let existingImg = this.findExistingImage(message.imageSrc);
            this.startEnhancementReact(existingImg);
        }
    }
}

let main = new Core.Main();
chrome.runtime.onMessage.addListener(
    (message) => main.onMessageListener(message)
);
