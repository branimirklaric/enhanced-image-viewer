namespace Core {
    export interface MousewheelKeys {
        scrollVerticallyKey: number;
        scrollHorizontallyKey: number;
        rotateImageKey: number;
    }
}
