namespace Core {
    export interface Message {
        startEnhancement: boolean,
        imageSrc: string
    }
}
