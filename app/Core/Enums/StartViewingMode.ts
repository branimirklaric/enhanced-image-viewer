﻿namespace Core.Enums {
    export enum StartViewingMode {
        ActualSize,
        FillWindow,
        FillWindowIfLarger
    }
}
