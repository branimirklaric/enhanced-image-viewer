﻿namespace Core.Enums {
    export enum ImagePosition {
        TopLeftCorner,
        Center
    }
}