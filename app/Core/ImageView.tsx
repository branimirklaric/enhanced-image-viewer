namespace Core {
    export interface ImageViewProps {
        image: HTMLImageElement,
        options: Options.Options
    }

    export class ImageView extends React.Component<ImageViewProps, {}> {
        //gets set when the image is loaded
        private image: HTMLImageElement;

        get naturalWidth() {
            return this.image.naturalWidth
        };

        get naturalHeight() {
            return this.image.naturalHeight
        };

        get imageAspectRatio() {
            return this.naturalWidth / this.naturalHeight;
        }

        get boundingBox() {
            return this.image.getBoundingClientRect();
        }

        get boundingBoxAspectRation() {
            return this.boundingBox.width / this.boundingBox.height;
        }

        get windowAspectRatio() {
            return window.innerWidth / window.innerHeight;
        }

        get fitsWindowWidth() {
            return window.innerWidth > this.naturalWidth;
        }

        get fitsWindowHeight() {
            return window.innerHeight > this.naturalHeight;
        }

        get diagonal() {
            return Math.sqrt(
                Math.pow(this.image.width, 2) + Math.pow(this.image.height, 2)
            );
        }

        newImageWidth(newDiagonal: number) {
            return Math.sqrt(
                Math.pow(newDiagonal, 2) / (Math.pow(this.imageAspectRatio, -2) + 1)
            );
        }

        newImageHeight(newDiagonal: number) {
            return Math.sqrt(
                Math.pow(newDiagonal, 2) / (Math.pow(this.imageAspectRatio, 2) + 1)
            );
        }

        get boundingBoxWidth() {
            return Math.round(this.boundingBox.width);
        }

        set boundingBoxWidth(newWidth) {
            var newDiagonal = (this.diagonal * newWidth / this.boundingBox.width);
            this.image.width = Math.round(this.newImageWidth(newDiagonal));
            this.image.height = this.newImageHeight(newDiagonal);
        }

        get boundingBoxHeight() {
            return Math.round(this.boundingBox.height);
        }

        set boundingBoxHeight(newHeight: number) {
            var newDiagonal = (this.diagonal * newHeight / this.boundingBox.height);
            this.image.width = this.newImageWidth(newDiagonal);
            this.image.height = Math.round(this.newImageHeight(newDiagonal));
        }

        private setScrollbarVisibility() {
            if (this.props.options.hideScrollbars) {
                document.body.style.overflow = "hidden";
            }
        }

        private setActualSize() {
            this.image.width = this.naturalWidth;
            this.image.height = this.naturalHeight;
        }

        private fillWindow() {
            if (this.boundingBoxAspectRation < this.windowAspectRatio) {
                this.boundingBoxHeight = window.innerHeight;
            } else {
                this.boundingBoxWidth = window.innerWidth;
            }
        }

        private fillWindowIfLarger() {
            if (this.fitsWindowHeight && this.fitsWindowWidth) {
                this.setActualSize();
            } else {
                this.fillWindow();
            }
        }

        private setStartViewingMode() {
            switch (this.props.options.startViewingMode) {
                case Enums.StartViewingMode.ActualSize: {
                    this.setActualSize();
                    break;
                }
                case Enums.StartViewingMode.FillWindow: {
                    this.fillWindow();
                    break;
                }
                case Enums.StartViewingMode.FillWindowIfLarger: {
                    this.fillWindowIfLarger();
                    break;
                }
                default: {
                    let never: never = this.props.options.startViewingMode;
                }
            }
        }

        get positionIsTopLeftCorner() {
            return (this.props.options.imagePosition
                === Enums.ImagePosition.TopLeftCorner
            );
        }

        positiveOrZero(number: number) {
            return (number > 0) ? number : 0;
        }

        get leftRightMargin() {
            return this.positiveOrZero((window.innerWidth - this.boundingBoxWidth) / 2);
        }

        get topBottomMargin() {
            return this.positiveOrZero((window.innerHeight - this.boundingBoxHeight) / 2);
        }

        private positionImage() {
            if (this.positionIsTopLeftCorner) {
                $(this.image).offset({ left: 0, top: 0 });
            }
            else {
                $(this.image).offset({
                    left: this.leftRightMargin,
                    top: this.topBottomMargin
                });
            }
        }

        private show() {
            this.image.style.visibility = "visible";
        }

        setUpPositioningCallback() {
            if (this.positionIsTopLeftCorner) {
                return;
            }
            window.addEventListener("resize", () => this.positionImage());
        }

        clearListeners() {
            $(this.image).off();
            $(document).off();
        }

        private distanceMoved = 0;
        private isMouseDown = false;
        private mousePointBeforeDrag = { x: 0, y: 0 };
        private dragged = false;

        onMousedown(mouseEvent: MouseEvent) {
            //preserves default Chrome ctrl + drag behavior
            if (mouseEvent.ctrlKey) {
                return;
            }
            mouseEvent.preventDefault();
            this.distanceMoved = 0;
            this.isMouseDown = true
            this.mousePointBeforeDrag = { x: mouseEvent.pageX, y: mouseEvent.pageY };
        }

        onMousemove(mouseEvent: MouseEvent) {
            if (mouseEvent.ctrlKey || !this.isMouseDown) {
                return;
            }
            mouseEvent.preventDefault();
            ++this.distanceMoved;
            if (this.distanceMoved < 3) {
                return;
            }
            this.dragged = true;
            window.scrollBy(
                this.mousePointBeforeDrag.x - mouseEvent.pageX,
                this.mousePointBeforeDrag.y - mouseEvent.pageY
            );
        }

        adjustScroll(
            percentX: number, percentY: number,
            clientX: number, clientY: number) {
            var newPageX = $(document).width() * percentX;
            var newPageY = $(document).height() * percentY;

            var newScrollLeft = newPageX - clientX;
            var newScrollTop = newPageY - clientY;

            $(window).scrollLeft(newScrollLeft);
            $(window).scrollTop(newScrollTop);
        }

        areAlmostSame(number: number, otherNumber: number) {
            const allowedDifference = 5;
            return Math.abs(number - otherNumber) < allowedDifference;
        }

        windowFilled() {
            let sameHeight = this.areAlmostSame(this.boundingBoxHeight, window.innerHeight);
            let sameWidth = this.areAlmostSame(this.boundingBoxWidth, window.innerWidth);
            return ((sameHeight && this.boundingBoxWidth <= window.innerWidth)
                || (sameWidth && this.boundingBoxHeight <= window.innerHeight)
            );
        }

        adjustScrollAfter<T extends MouseEvent>(
            mouseEvent: T,
            sizeChangingFunction: (mouseEvent: T) => void) {
            var percentXBeforeResize = mouseEvent.pageX / $(document).width();
            var percentYBeforeResize = mouseEvent.pageY / $(document).height();

            sizeChangingFunction(mouseEvent);

            this.adjustScroll(
                percentXBeforeResize, percentYBeforeResize,
                mouseEvent.clientX, mouseEvent.clientY
            );
        }

        imageClicked(mouseEvent: MouseEvent) {
            let mouseXInsideImage = mouseEvent.pageX - this.boundingBox.left;
            let mouseYInsideImage = mouseEvent.pageY - this.boundingBox.top;

            let percentXBeforeResize = mouseXInsideImage / this.boundingBoxWidth;
            let percentYBeforeResize = mouseYInsideImage / this.boundingBoxHeight;

            if (this.windowFilled()) {
                this.setActualSize();
            } else {
                this.fillWindow();
            }
            this.positionImage();

            this.adjustScroll(
                percentXBeforeResize, percentYBeforeResize,
                mouseEvent.clientX, mouseEvent.clientY
            );
        }

        onMouseup(mouseEvent: MouseEvent) {
            mouseEvent.preventDefault();
            this.isMouseDown = false;
            if (!this.dragged && mouseEvent.which === 1) {
                this.imageClicked(mouseEvent);
            } else {
                this.dragged = false;
            }
        }

        private scrollVerticallyKeyPressed = false;
        private scrollHorizontallyKeyPressed = false;
        private rotateImageKeyPressed = false;

        setMousewheelKeysPressed(keyboardEvent: KeyboardEvent) {
            let isPressed = (keyboardEvent.type === "keydown");
            let mousewheelKeys = this.props.options.mousewheelKeys;
            if (keyboardEvent.which === mousewheelKeys.scrollHorizontallyKey) {
                keyboardEvent.preventDefault();
                this.scrollHorizontallyKeyPressed = isPressed;
            }
            else if (keyboardEvent.which === mousewheelKeys.scrollVerticallyKey) {
                keyboardEvent.preventDefault();
                this.scrollVerticallyKeyPressed = isPressed;
            }
            else if (keyboardEvent.which === mousewheelKeys.rotateImageKey) {
                keyboardEvent.preventDefault();
                this.rotateImageKeyPressed = isPressed;
            }
        }

        private degreesRotated = 0;
        private flippedHorizontally = false;
        private flippedVertically = false;

        private readonly normalHorizontalScale = "scaleX(1)";
        private readonly flippedHorizontalScale = "scaleX(-1)";
        private readonly normalVerticalScale = "scaleY(1)";
        private readonly flippedVerticalScale = "scaleY(-1)";

        get scaleX() {
            return (
                this.flippedHorizontally
                    ? this.flippedHorizontalScale
                    : this.normalHorizontalScale
            );
        }

        get scaleY() {
            return (
                this.flippedVertically
                    ? this.flippedVerticalScale
                    : this.normalVerticalScale
            );
        }

        get transform(): [string, string, string] {
            return [`rotate(${this.degreesRotated}deg)`, this.scaleX, this.scaleY];
        }

        updateTransform(createNewTransform:
            (oldTransform: [string, string, string]) => [string, string, string]) {
            let [newRotate, newScaleX, newScaleY] = createNewTransform(this.transform);
            this.image.style.transform = `${newRotate} ${newScaleX} ${newScaleY}`;
        }

        rotate(degrees: number) {
            this.updateTransform(([oldRotate, oldScaleX, oldScaleY]) => {
                this.degreesRotated += degrees;
                let newRotate = `rotate(${this.degreesRotated}deg)`;
                return [newRotate, oldScaleX, oldScaleY];
            });
        }

        scale(deltaY: number) {
            let newHeight = this.image.height * Math.pow(1.1, -Math.sign(deltaY));
            let newWidth = newHeight * this.imageAspectRatio;
            if (newHeight >= 10 && newWidth >= 10) {
                this.image.height = newHeight;
                this.image.width = newWidth;
            }
        }

        onMousewheel(wheelEvent: WheelEvent) {
            wheelEvent.preventDefault();
            if (this.rotateImageKeyPressed) {
                this.adjustScrollAfter(wheelEvent, (wheelEvent) => {
                    this.rotate(Math.sign(wheelEvent.deltaY) * 5);
                    this.positionImage();
                });
            }
            else if (this.scrollHorizontallyKeyPressed) {
                // uses deltaX for shift + mousewheel, deltaY for allElse + mousewheel
                var deltaXY = (wheelEvent.deltaX === 0
                    ? wheelEvent.deltaY : wheelEvent.deltaX
                );
                window.scrollBy(Math.sign(deltaXY) * 80, 0);
            }
            else {
                let isDefaultZoomMode =
                    this.props.options.zoomMode === Core.Enums.ZoomMode.Default;
                let isEnhancedZoomMode =
                    this.props.options.zoomMode === Core.Enums.ZoomMode.Enhanced;
                let shouldScrollVertically =
                    (this.scrollVerticallyKeyPressed && isEnhancedZoomMode)
                    || (!this.scrollVerticallyKeyPressed && isDefaultZoomMode);
                if (shouldScrollVertically) {
                    window.scrollBy(0, Math.sign(wheelEvent.deltaY) * 80);
                }
                else {
                    this.adjustScrollAfter(wheelEvent, (wheelEvent) => {
                        this.scale(wheelEvent.deltaY);
                        this.positionImage();
                    });
                }
            }
        }

        unpressAllMousewheelKeys() {
            this.scrollVerticallyKeyPressed = false;
            this.scrollHorizontallyKeyPressed = false;
            this.rotateImageKeyPressed = false;
        }

        setUpMouseCallbacks() {
            this.clearListeners();
            this.image.addEventListener("mousedown", e => this.onMousedown(e));
            this.image.addEventListener("mousemove", e => this.onMousemove(e));
            this.image.addEventListener("mouseup", e => this.onMouseup(e));

            document.addEventListener("keydown", e => this.setMousewheelKeysPressed(e));
            document.addEventListener("keyup", e => this.setMousewheelKeysPressed(e));
            document.addEventListener("wheel", e => this.onMousewheel(e));

            window.addEventListener("blur", _ => this.unpressAllMousewheelKeys());
        }

        flipHorizontally() {
            this.updateTransform(([oldRotate, oldScaleX, oldScaleY]) => {
                let newScaleX = (
                    this.flippedHorizontally
                        ? this.normalHorizontalScale
                        : this.flippedHorizontalScale
                );
                this.flippedHorizontally = !this.flippedHorizontally;
                return [oldRotate, newScaleX, oldScaleY];
            });
        }

        flipVertically() {
            this.updateTransform(([oldRotate, oldScaleX, oldScaleY]) => {
                let newScaleY = (
                    this.flippedVertically
                        ? this.normalVerticalScale
                        : this.flippedVerticalScale
                );
                this.flippedVertically = !this.flippedVertically;
                return [oldRotate, oldScaleX, newScaleY];
            });
        }

        setUpKeyboardCallback() {
            document.addEventListener("keydown", (keyboardEvent) => {
                const hKey = 72;
                const vKey = 86;
                const qKey = 81;
                const eKey = 69;
                if (keyboardEvent.which === hKey) {
                    this.flipHorizontally();
                }
                else if (keyboardEvent.which === vKey) {
                    this.flipVertically();
                }
                else if (keyboardEvent.which === qKey) {
                    this.rotate(-90);
                }
                else if (keyboardEvent.which === eKey) {
                    this.rotate(90);
                }
            });
        }

        enhance(image: HTMLImageElement) {
            this.image = image;
            this.setScrollbarVisibility();
            this.setStartViewingMode();
            this.positionImage();
            this.show();

            this.setUpPositioningCallback();
            this.setUpMouseCallbacks();
            this.setUpKeyboardCallback();
        }

        public render() {
            return (
                <img
                    src={this.props.image.src}
                    onLoad={e => this.enhance(e.currentTarget)}
                    style={{ visibility: "hidden", display: "block" }}
                />
            );
        }
    }
}
