﻿namespace Background {
    /** Background tasks related to enhancing images opened from files. */
    export class BackgroundFileTab {
        constructor(
            private scriptInjector: Misc.ScriptInjector) {
        }

        private setUpOnUpdatedListener() {
            chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
                if (changeInfo.status !== "complete") {
                    return;
                }
                if (tab.url == undefined) {
                    return;
                }
                if (tab.url.indexOf("file") !== 0) {
                    return;
                }
                this.scriptInjector.injectScripts(
                    tabId,
                    () => {
                        chrome.tabs.sendMessage(tabId, {
                            startEnhancement: true,
                            imageSrc: tab.url
                        });
                    }
                );
            });
        }

        setUpViewingInFileTab() {
            this.setUpOnUpdatedListener();
        }
    }
}
