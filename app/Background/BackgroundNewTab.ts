﻿import CWR = chrome.webRequest

namespace Background {
    /**Background tasks related to enhancing images opened in new tabs.
     * The tab is marked for enhancement when the headers are recieved and enhanced after it is fully loaded.
     */
    export class BackgroundNewTab {
        /** Tabs that have been marked for enhancement. */
        private tabsToEnhanceIds: Array<number> = [];

        constructor(
            private scriptInjector: Misc.ScriptInjector) {
        }

        /** Checks whether to mark the tab for enhancement or not. */
        private headersReceivedCallback(details: CWR.WebResponseHeadersDetails) {
            if ((details.statusCode !== 200) || !(details.responseHeaders)) {
                return {};
            }

            let headerNamesAndValues = _.map(
                details.responseHeaders,
                function (header): [string, string] {
                    return [header.name.toLowerCase(), header.value as string];
                }
            );

            let [isImage, beingDownloaded] = _.reduce(
                headerNamesAndValues,
                function ([isImage, beingDownloaded], [headerName, headerValue]) {
                    if (headerName.localeCompare("content-type") === 0
                        && headerValue.indexOf("image") > -1) {
                        isImage = true;
                    }
                    if (headerName.localeCompare("content-disposition") === 0
                        && headerValue.indexOf("attachment") > -1) {
                        beingDownloaded = true;
                    }
                    return [isImage, beingDownloaded];
                },
                [false, false]
            );

            if (isImage && !beingDownloaded) {
                this.tabsToEnhanceIds.push(details.tabId);
            }

            return {};
        }

        private addOnHeadersRecievedListener() {
            CWR.onHeadersReceived.addListener(
                this.headersReceivedCallback.bind(this),
                { urls: ["*://*/*"], types: ["main_frame"] },
                ["responseHeaders", "blocking"] //I probably don't need blocking
            );
        }

        /** Sends enhancement start message to the tab if the tab has been marked for enhancement. */
        private onCompletedCallback(details: CWR.WebResponseCacheDetails) {
            let tabIdIndex = this.tabsToEnhanceIds.indexOf(details.tabId);
            if (tabIdIndex === -1) {
                return;
            }

            this.scriptInjector.injectScripts(
                this.tabsToEnhanceIds[tabIdIndex],
                () => {
                    chrome.tabs.get(details.tabId, (tab) => {
                        chrome.tabs.sendMessage(details.tabId, {
                            startEnhancement: true,
                            imageSrc: tab.url
                        });
                    })
                }
            );

            this.tabsToEnhanceIds.splice(tabIdIndex, 1);
        }

        private addOnCompletedListener() {
            CWR.onCompleted.addListener(
                this.onCompletedCallback.bind(this),
                { urls: ["*://*/*"], types: ["main_frame"] }
            );
        }

        setUpViewingInNewTab() {
            this.addOnHeadersRecievedListener();
            this.addOnCompletedListener();
        }
    }
}
