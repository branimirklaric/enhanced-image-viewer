﻿namespace Background {
    export class Main {
        /**
         * Scripts needed to enhance the image being viewed.
         */
        private static readonly enhancementScripts: Array<chrome.tabs.InjectDetails> = [
            { file: "Libs/jquery-3.1.1.min.js" },
            { file: "Libs/jquery.mousewheel.min.js" },
            { file: "Libs/lodash.min.js" },
            { file: "Libs/react.js" },
            { file: "Libs/react-dom.js" },

            { file: "Core/Enums/StartViewingMode.js" },
            { file: "Core/Enums/ImagePosition.js" },
            { file: "Core/Enums/ZoomMode.js" },

            { file: "Options/Options.js" },
            { file: "Options/OptionsRepository.js" },

            { file: "Core/ImageView.js" },

            { file: "Core/Main.js" }
        ];

        static showOptionsOnInstallOrUpdate() {
            chrome.runtime.onInstalled.addListener((details) => {
                if (details.reason === "install" /*|| details.reason === "update"*/) {
                    chrome.tabs.create({
                        "url": `chrome://extensions/?options=${chrome.runtime.id}`
                    });
                }
            });
        }

        static setUninstallUrl() {
            chrome.runtime.setUninstallURL("https://goo.gl/forms/293J8zvj3mry3FNh1");
        }

        static setUpBackgroundScripts() {
            new Options.OptionsRepository(chrome.storage.sync).findAllOptions(options => {
                let scriptInjector = new Misc.ScriptInjector(
                    chrome.tabs.executeScript, Main.enhancementScripts
                );
                new BackgroundSameTab(scriptInjector, options.showContextMenu)
                    .setUpViewingInSameTab();
                new BackgroundNewTab(scriptInjector)
                    .setUpViewingInNewTab();
                new BackgroundFileTab(scriptInjector)
                    .setUpViewingInFileTab();
            });
        }

        /**
         * Extensions "entry point".
         */
        static main() {
            this.showOptionsOnInstallOrUpdate();
            this.setUninstallUrl();
            this.setUpBackgroundScripts();
        }
    }
}

Background.Main.main();
