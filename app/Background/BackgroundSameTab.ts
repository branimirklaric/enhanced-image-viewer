﻿namespace Background {
    /** Background tasks related to enhancing images viewed in the current tab. */
    export class BackgroundSameTab {
        constructor(
            private scriptInjector: Misc.ScriptInjector,
            private showContextMenu: boolean) {
        }

        private addContextMenu() {
            if (this.showContextMenu) {
                chrome.contextMenus.create({
                    id: "EnhancedImageViewer",
                    title: "View image in current tab",
                    contexts: ["image"]
                });
            }
        }

        /** Sends enhancement start message to the tab if the context menu has been clicked. */
        private addContextMenuListener() {
            chrome.contextMenus.onClicked.addListener((info, tab) => {
                if (tab === undefined || tab.url === undefined || tab.id === undefined) {
                    return
                };
                if (tab.url.indexOf("chrome") === 0 ||
                    tab.url.indexOf("data") === 0) {
                    alert("I can't let you do that, Tim.");
                    return;
                }
                //This only fires on context menus created by this extension
                //and since I only have one, I don't have to check
                //the context menu item id.
                let tabId = tab.id;
                this.scriptInjector.injectScripts(
                    tabId,
                    () => {
                        chrome.tabs.sendMessage(tabId, {
                            startEnhancement: true,
                            imageSrc: info.srcUrl
                        });
                    }
                );
            });
        }

        private removeContextMenu() {
            chrome.contextMenus.removeAll();
        }

        private addShowContextMenuLister() {
            chrome.runtime.onMessage.addListener(message => {
                if (message.showContextMenu === undefined) {
                    return;
                }
                if (message.showContextMenu) {
                    this.addContextMenu();
                }
                else {
                    this.removeContextMenu();
                }
            })
        }

        setUpViewingInSameTab() {
            this.addContextMenu();
            this.addContextMenuListener();
            this.addShowContextMenuLister();
        }
    }
}
