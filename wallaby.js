module.exports = function (wallaby) {
    return {
        files: [
            "app/Options/OptionsPage.html",
            { pattern: "app/Libs/jquery-3.1.1.min.js", instrument: false },
            { pattern: "app/Libs/jquery.mousewheel.min.js", instrument: false },
            { pattern: "app/Libs/jscolor.min.js", instrument: false },
            { pattern: "app/Libs/lodash.min.js", instrument: false },

            { pattern: "app/Libs/react.min.js", instrument: false },
            { pattern: "app/Libs/react-dom.min.js", instrument: false },

            "app/Misc/**/*.ts",

            "app/Core/**/*.ts",
            "!app/Core/Main.ts",

            "app/Options/**/*.ts",
            "app/Options/**/*.tsx",
            "!app/Options/Main.tsx",
            "!app/Options/OptionsReact.tsx",

            { pattern: "node_modules/sinon/pkg/sinon.js", instrument: false },
            { pattern: "node_modules/chai/chai.js", instrument: false },
            { pattern: "test/SinonHelpers.ts", instrument: false },
            { pattern: "test/Options/View/SetupHelpers.ts", instrument: false }
        ],
        tests: [
            "test/**/*Spec.ts",
            "test/**/*Spec.tsx"
        ]
    };
};
