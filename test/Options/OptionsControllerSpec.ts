describe("Find all options", () => {
    it("should call options repository find all options", () => {
        let optionsRepository = <Options.OptionsRepository>{};
        let findSpy = sinon.spy();
        optionsRepository.findAllOptions = findSpy;
        let optionsController = new Options.OptionsController(optionsRepository);
        let callback = (option: Options.Options) => { };

        optionsController.findAllOptions(callback);

        calledOnceWith(findSpy, callback);
    })
})

describe("Update background color", () => {
    it("should call options repository update background color", () => {
        let optionsRepository = <Options.OptionsRepository>{};
        let updateSpy = sinon.spy();
        optionsRepository.updateBackgroundColor = updateSpy;
        let optionsController = new Options.OptionsController(optionsRepository);
        let backgroundColor = "asdf";

        optionsController.updateBackgroundColor(backgroundColor);

        calledOnceWith(updateSpy, backgroundColor);
    })
})

describe("Update start viewing mode", () => {
    it("should call options repository update start viewing mode", () => {
        let optionsRepository = <Options.OptionsRepository>{};
        let updateSpy = sinon.spy();
        optionsRepository.updateStartViewingMode = updateSpy;
        let optionsController = new Options.OptionsController(optionsRepository);
        let startViewingMode = Core.Enums.StartViewingMode.FillWindow.toString();

        optionsController.updateStartViewingMode(startViewingMode);

        calledOnceWith(updateSpy, Core.Enums.StartViewingMode.FillWindow);
    })
})

describe("Update image position", () => {
    it("should call options repository update image position", () => {
        let optionsRepository = <Options.OptionsRepository>{};
        let updateSpy = sinon.spy();
        optionsRepository.updateImagePosition = updateSpy
        let optionsController = new Options.OptionsController(optionsRepository);
        let oldPosition = Core.Enums.ImagePosition.Center;

        optionsController.updateImagePosition(oldPosition);

        calledOnceWith(updateSpy, Core.Enums.ImagePosition.TopLeftCorner);
    })
})

describe("Update hide scrollbars", () => {
    it("should call options repository update hide scrollbars", () => {
        let optionsRepository = <Options.OptionsRepository>{};
        let updateSpy = sinon.spy();
        optionsRepository.updateHideScrollbars = updateSpy;
        let optionsController = new Options.OptionsController(optionsRepository);
        let oldHideScrollbars = false;

        optionsController.updateHideScrollbars(oldHideScrollbars);

        calledOnceWith(updateSpy, true);
    })
})

describe("Update mousewheel keys", () => {
    const oldKeys: Core.MousewheelKeys = {
        rotateImageKey: 0,
        scrollHorizontallyKey: 1,
        scrollVerticallyKey: 2
    };
    const newKey = 3;

    it("update vetically should call update mousewheel keys", () => {
        let optionsRepository = <Options.OptionsRepository>{};
        let updateSpy = sinon.spy();
        optionsRepository.updateMousewheelKeys = updateSpy;
        let optionsController = new Options.OptionsController(optionsRepository);

        optionsController.updateVerticallyKey(oldKeys, newKey);

        calledOnceWith(updateSpy, { ...oldKeys, scrollVerticallyKey: newKey });
    })

    it("update horizontally should call update mousewheel keys", () => {
        let optionsRepository = <Options.OptionsRepository>{};
        let updateSpy = sinon.spy();
        optionsRepository.updateMousewheelKeys = updateSpy;
        let optionsController = new Options.OptionsController(optionsRepository);

        optionsController.updateHorizontallyKey(oldKeys, newKey);

        calledOnceWith(updateSpy, { ...oldKeys, scrollHorizontallyKey: newKey });
    })

    it("update rotate should call update mousewheel keys", () => {
        let optionsRepository = <Options.OptionsRepository>{};
        let updateSpy = sinon.spy();
        optionsRepository.updateMousewheelKeys = updateSpy;
        let optionsController = new Options.OptionsController(optionsRepository);

        optionsController.updateRotateKey(oldKeys, newKey);

        calledOnceWith(updateSpy, { ...oldKeys, rotateImageKey: newKey });
    })
})
