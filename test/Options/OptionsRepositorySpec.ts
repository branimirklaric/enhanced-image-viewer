function syncStorageSpy() {
    let syncStorage = <chrome.storage.SyncStorageArea>{};
    syncStorage.set = sinon.spy();
    syncStorage.get = sinon.spy();
    return syncStorage;
}

function optionsRepositoryAndSetSpy()
    : [Options.OptionsRepository, sinon.SinonSpy] {
    let syncStorage = syncStorageSpy();
    let optionsRepository = new Options.OptionsRepository(syncStorage);
    return [optionsRepository, <sinon.SinonSpy>syncStorage.set];
}

function optionsRepositoryAndGetSpy()
    : [Options.OptionsRepository, sinon.SinonSpy] {
    let syncStorage = syncStorageSpy();
    let optionsRepository = new Options.OptionsRepository(syncStorage);
    return [optionsRepository, <sinon.SinonSpy>syncStorage.get];
}

describe("Find all options", () => {
    it("returns default options", () => {
        let [optionsRepository, storageGetSpy] = optionsRepositoryAndGetSpy();
        let callback = (option: Options.Options) => { };

        optionsRepository.findAllOptions(callback);

        calledOnceWith(
            storageGetSpy, Options.OptionsRepository.defaultOptions, callback
        );
    })
})

describe("Update background color", () => {
    it("should call sync.set", () => {
        let [optionsRepository, storageSetSpy] = optionsRepositoryAndSetSpy();
        let backgroundColor = "asdf"

        optionsRepository.updateBackgroundColor(backgroundColor);

        calledOnceWith(storageSetSpy, { backgroundColor: backgroundColor });
    })
})

describe("Update start viewing mode", () => {
    it("should call sync.set", () => {
        let [optionsRepository, storageSetSpy] = optionsRepositoryAndSetSpy();
        let startViewingMode = Core.Enums.StartViewingMode.FillWindowIfLarger

        optionsRepository.updateStartViewingMode(startViewingMode);

        calledOnceWith(storageSetSpy, { startViewingMode: startViewingMode });
    })
})

describe("Update image position", () => {
    it("should call sync.set", () => {
        let [optionsRepository, storageSetSpy] = optionsRepositoryAndSetSpy();
        let imagePosition = Core.Enums.ImagePosition.Center;

        optionsRepository.updateImagePosition(imagePosition);

        calledOnceWith(storageSetSpy, { imagePosition: imagePosition });
    })
})

describe("Update hide scrollbars", () => {
    it("should call sync.set", () => {
        let [optionsRepository, storageSetSpy] = optionsRepositoryAndSetSpy();
        let hideScrollbars = true;

        optionsRepository.updateHideScrollbars(hideScrollbars);

        calledOnceWith(storageSetSpy, { hideScrollbars: hideScrollbars });
    })
})

describe("Update mousewheel keys", () => {
    it("should call sync.set", () => {
        let [optionsRepository, storageSetSpy] = optionsRepositoryAndSetSpy();
        let mousewheelKeys = Options.OptionsRepository.defaultOptions.mousewheelKeys;

        optionsRepository.updateMousewheelKeys(mousewheelKeys);

        calledOnceWith(storageSetSpy, { mousewheelKeys: mousewheelKeys });
    })
})
