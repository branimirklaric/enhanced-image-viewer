describe("Options view", () => {
    it("should properly initalize all components.", () => {
        let controller: Options.OptionsController = {} as Options.OptionsController;
        controller.updateBackgroundColor = sinon.spy();
        controller.updateImagePosition = sinon.spy();
        controller.updateHideScrollbars = sinon.spy();
        controller.updateStartViewingMode = sinon.spy();
        controller.updateVerticallyKey = sinon.spy();
        controller.updateHorizontallyKey = sinon.spy();
        controller.updateRotateKey = sinon.spy();
        let mousewheelKeys = {
            scrollVerticallyKey: 1,
            scrollHorizontallyKey: 2,
            rotateImageKey: 3
        };
        let options: Options.Options = {
            backgroundColor: "blue",
            hideScrollbars: true,
            imagePosition: Core.Enums.ImagePosition.Center,
            startViewingMode: Core.Enums.StartViewingMode.FillWindow,
            mousewheelKeys: mousewheelKeys,
            zoomMode: Core.Enums.ZoomMode.Enhanced,
            showContextMenu: true
        };
        let view = <Options.OptionsView
            optionsController={controller}
            options={options}
        />
        let renderedView = ReactDOM.render(view, document.createElement("div")) as Options.OptionsView;

        renderedView.backgroundColor.props.updateBackgroundColor("blue");
        calledOnceWith(controller.updateBackgroundColor as sinon.SinonSpy, "blue");

        renderedView.hideScrollbars.props.updateHideScrollbars(true);
        calledOnceWith(controller.updateHideScrollbars as sinon.SinonSpy, true);

        renderedView.imagePosition.props.updateImagePosition(Core.Enums.ImagePosition.Center);
        calledOnceWith(controller.updateImagePosition as sinon.SinonSpy, Core.Enums.ImagePosition.Center);

        renderedView.startViewingMode.props.updateStartViewingMode(Core.Enums.StartViewingMode.FillWindow.toString());
        calledOnceWith(controller.updateStartViewingMode as sinon.SinonSpy, Core.Enums.StartViewingMode.FillWindow.toString());

        renderedView.mousewheelKeys.props.updateVerticallyKey(mousewheelKeys, 1);
        calledOnceWith(controller.updateVerticallyKey as sinon.SinonSpy, mousewheelKeys, 1);

        renderedView.mousewheelKeys.props.updateHorizontallyKey(mousewheelKeys, 2);
        calledOnceWith(controller.updateHorizontallyKey as sinon.SinonSpy, mousewheelKeys, 2);

        renderedView.mousewheelKeys.props.updateRotateKey(mousewheelKeys, 3);
        calledOnceWith(controller.updateRotateKey as sinon.SinonSpy, mousewheelKeys, 3);

        chai.expect(renderedView.backgroundColor.props.backgroundColor).to.equal("blue");
        chai.expect(renderedView.hideScrollbars.props.scrollbarsHidden).to.equal(true);
        chai.expect(renderedView.imagePosition.props.checkedPosition).to.equal(Core.Enums.ImagePosition.Center);
        chai.expect(renderedView.startViewingMode.props.startViewingMode).to.equal(Core.Enums.StartViewingMode.FillWindow);
        chai.expect(renderedView.mousewheelKeys.props.mousewheelKeys).to.equal(mousewheelKeys);
    })
})
