describe("Mousewheel keys", () => {
    it("should have the inital values set in props.", () => {
        document.body.innerHTML = "";
        let props = {
            mousewheelKeys: {
                scrollVerticallyKey: 65,
                scrollHorizontallyKey: 66,
                rotateImageKey: 67
            },
            updateVerticallyKey: sinon.spy(),
            updateHorizontallyKey: sinon.spy(),
            updateRotateKey: sinon.spy(),
            textIndent: "1.5em"
        }
        let mousewheelKeys = <Options.View.MousewheelKeysComponent {...props} />;
        ReactDOM.render(mousewheelKeys, addContainerToDocument());

        let inputs = document.getElementsByTagName("input");
        let vertically = inputs[0];
        let horizontally = inputs[1];
        let rotate = inputs[2];

        chai.expect(vertically.value).to.equal("a");
        chai.expect(horizontally.value).to.equal("b");
        chai.expect(rotate.value).to.equal("c");
    })

    it("should call the on change callbacks when changed.", () => {
        document.body.innerHTML = "";
        let props = {
            mousewheelKeys: {
                scrollVerticallyKey: 65,
                scrollHorizontallyKey: 66,
                rotateImageKey: 67
            },
            updateVerticallyKey: sinon.spy(),
            updateHorizontallyKey: sinon.spy(),
            updateRotateKey: sinon.spy(),
            textIndent: "1.5em"
        }
        let mousewheelKeys = <Options.View.MousewheelKeysComponent {...props} />;
        ReactDOM.render(mousewheelKeys, addContainerToDocument());

        let inputs = document.getElementsByTagName("input");
        let vertically = inputs[0];
        vertically.dispatchEvent(new Event("keydown", { bubbles: true, which: 68 } as EventInit));

        let horizontally = inputs[1];
        horizontally.dispatchEvent(new Event("keydown", { bubbles: true, which: 69 } as EventInit));

        let rotate = inputs[2];
        rotate.dispatchEvent(new Event("keydown", { bubbles: true, which: 70 } as EventInit));

        sinon.assert.calledOnce(props.updateVerticallyKey);
        sinon.assert.calledOnce(props.updateHorizontallyKey);
        sinon.assert.calledOnce(props.updateRotateKey);

        //can't send which property; get read as undefined
        // chai.expect(vertically.value).to.equal("d");
        // chai.expect(horizontally.value).to.equal("e");
        // chai.expect(rotate.value).to.equal("f");
    })
})
