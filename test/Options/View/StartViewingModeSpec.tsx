describe("Start viewing mode component", () => {
    function createStartViewingMode()
        : [JSX.Element, Options.View.StartViewingModeProps] {
        let props: Options.View.StartViewingModeProps = {
            startViewingMode: Core.Enums.StartViewingMode.FillWindow,
            updateStartViewingMode: sinon.spy()
        };
        let startViewingMode = <Options.View.StartViewingModeComponent {...props} />;
        return [startViewingMode, props];
    }

    it("should have the inital value set in props.", () => {
        let [startViewingMode, _] = createStartViewingMode();
        let container = addContainerToDocument();
        ReactDOM.render(startViewingMode, container);

        let select = document.getElementsByTagName("select")[0];

        chai.expect(select.value).to.equal("1");

        //because it isn't cleaned up between tests
        document.body.innerHTML = "";
    });

    it("should call the on change callback when changed.", () => {
        let [startViewingMode, props] = createStartViewingMode();
        let container = addContainerToDocument();
        ReactDOM.render(startViewingMode, container);

        let select = document.getElementsByTagName("select")[0];
        select.value = "2";
        select.dispatchEvent(new Event("change", { bubbles: true }));

        calledOnceWith(props.updateStartViewingMode as sinon.SinonSpy, "2");
    });
});
