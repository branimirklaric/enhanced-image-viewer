describe("Image position component", () => {
    it("should have inital value set in props.", () => {
        document.body.innerHTML = "";
        let center = Core.Enums.ImagePosition.Center;
        let props = {
            checkedPosition: center,
            updateImagePosition: sinon.spy()
        };
        let imagePosition = <Options.View.ImagePositionComponent {...props} />;
        ReactDOM.render(imagePosition, addContainerToDocument());

        let checkedRadio = document.getElementsByTagName("input").item(center);

        chai.expect(checkedRadio.checked).to.be.true;
    });

    it("should change value on change event and call update.", () => {
        document.body.innerHTML = "";
        let props = {
            checkedPosition: Core.Enums.ImagePosition.Center,
            updateImagePosition: sinon.spy()
        };
        let imagePosition = <Options.View.ImagePositionComponent {...props} />;
        ReactDOM.render(imagePosition, addContainerToDocument());

        let uncheckedRadio = document.getElementsByTagName("input").item(
            Core.Enums.ImagePosition.TopLeftCorner
        );
        uncheckedRadio.dispatchEvent(new Event("click", { bubbles: true }));

        chai.expect(uncheckedRadio.checked).to.be.true;
        sinon.assert.calledOnce(props.updateImagePosition);
    });
});
