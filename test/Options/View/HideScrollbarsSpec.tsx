describe("Hide scrollbars", () => {
    it("should have the inital value set in props.", () => {
        document.body.innerHTML = "";
        let props = {
            scrollbarsHidden: true,
            updateHideScrollbars: sinon.spy()
        };
        let hideScrollbars = <Options.View.HideScrollbarsComponent {...props} />;
        ReactDOM.render(hideScrollbars, addContainerToDocument());

        let input = document.getElementsByTagName("input")[0];

        chai.expect(input.checked).to.be.true;
    })

    it("should call the on change callback when changed.", () => {
        document.body.innerHTML = "";
        let props = {
            scrollbarsHidden: true,
            updateHideScrollbars: sinon.spy()
        };
        let hideScrollbars = <Options.View.HideScrollbarsComponent {...props} />;
        ReactDOM.render(hideScrollbars, addContainerToDocument());

        let input = document.getElementsByTagName("input")[0];
        input.dispatchEvent(new Event("click", { bubbles: true }));

        chai.expect(input.checked).to.be.false;
        calledOnceWith(props.updateHideScrollbars, true);
    })
})
