describe("Background color component", () => {
    it("should have the inital value set in props", () => {
        let props: Options.View.BackgroundColorProps = {
            backgroundColor: "000000",
            updateBackgroundColor: sinon.spy()
        };
        let backgroundColor = <Options.View.BackgroundColorComponent {...props} />;
        ReactDOM.render(backgroundColor, addContainerToDocument());
        
        let input = document.getElementsByTagName("input")[0];

        chai.expect(input.value).to.equal("000000");

        //because it isn't cleaned up between tests
        document.body.innerHTML = "";
    });

    it("should call the on change callback when changed.", () => {
        let props: Options.View.BackgroundColorProps = {
            backgroundColor: "000000",
            updateBackgroundColor: sinon.spy()
        };
        let backgroundColor = <Options.View.BackgroundColorComponent {...props} />;
        ReactDOM.render(backgroundColor, addContainerToDocument());

        let input = document.getElementsByTagName("input")[0];
        input.value = "aabbcc";
        input.dispatchEvent(new Event("change", { bubbles: true }));

        calledOnceWith(props.updateBackgroundColor as sinon.SinonSpy, "aabbcc");
    });
})
