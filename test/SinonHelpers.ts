function calledOnceWith(spy: sinon.SinonSpy, ...args: any[]) {
    sinon.assert.calledOnce(spy);
    sinon.assert.calledWith(spy, ...args)
}
