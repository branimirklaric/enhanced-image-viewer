describe("Scripts executer", () => {
    it("should create a function that injects a file with the callback and call that.", () => {
        let files = [{ file: "1" }];
        let result: string[] = [];
        let injector = new Misc.ScriptInjector((tabId, script, callback) => {
            result.push(script.file as string);
            callback();
        }, files);
        injector.injectScripts(0, () => {
            result.push("Done")
        });
        chai.expect(result).to.deep.equal(["1", "Done"]);
    })

    it("should create a function that injects multiple files with the callback and call that.", () => {
        let files = [{ file: "1" }, { file: "2" }, { file: "3" }];
        let result: string[] = [];
        let injector = new Misc.ScriptInjector((tabId, script, callback) => {
            result.push(script.file as string);
            callback();
        }, files);
        injector.injectScripts(0, () => {
            result.push("Done")
        });
        console.log(result);
        chai.expect(result).to.deep.equal(["1", "2", "3", "Done"]);
    })

    it("should just call the callback if the scripts array is empty.", () => {
        let files: chrome.tabs.InjectDetails[] = [];
        let result: string[] = [];
        let injector = new Misc.ScriptInjector((tabId, script, callback) => {
            //Is never called. chai.expect(true).to.equal(false);
        }, files);
        injector.injectScripts(0, () => {
            result.push("Done")
        });
        console.log(result);
        chai.expect(result).to.deep.equal(["Done"]);
    })
})
